<?php

namespace GrizzDev\AMD;

use Illuminate\Support\ServiceProvider;

class AMDServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot() {
		$this->publishes([
			__DIR__.'/../../public/assets' => public_path('assets'),
		], 'public');

		$this->publishes([
			__DIR__.'/../../resources/assets' => base_path('resources/assets'),
		], 'resources');

		$this->publishes([
			__DIR__.'/../../config.php' => config_path('gdamd.php'),
		], 'config');

		$this->app->singleton('grizzdev.amd.asset', function() {
			return new Classes\Asset;
		});

		$this->app->singleton('grizzdev.amd.manifest', function() {
			return new Classes\Manifest;
		});

		$this->commands('grizzdev.amd.manifest.command');

		$this->app->singleton('grizzdev.amd.manifest.command', function() {
			return new Commands\AMDManifestCommand;
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register() {
		$this->mergeConfigFrom(
			__DIR__.'/../../config.php', 'gdamd'
		);
	}

	public function provides() {
		return [
			'grizzdev.amd.asset',
			'grizzdev.amd.manifest',
			'grizzdev.amd.manifest.command'
		];
	}

}
