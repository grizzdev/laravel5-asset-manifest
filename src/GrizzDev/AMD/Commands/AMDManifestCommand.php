<?php

namespace GrizzDev\AMD\Commands;

use Illuminate\Console\Command;
use GrizzDev\AMD\Classes\Manifest;

class AMDManifestCommand extends Command {

	protected $name = "grizzdev:manifest";

	protected $signature = "grizzdev:manifest {file : Filename in config('gdamd.paths.resources.[type]')} {type : either 'css' or 'js'}";

	protected $description = 'Calls GrizzDev\AMD\Classes\Manifest::make(..)';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	/*public function __construct() {
	}*/

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle() {
		$file = $this->argument('file');
		$type = $this->argument('type');

		if($result = Manifest::make($file, $type)) {
			$this->info($result);
		} else {
			$this->error("Unable to create $type manifest for $file");
		}
	}

}
