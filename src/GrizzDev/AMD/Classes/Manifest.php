<?php

namespace GrizzDev\AMD\Classes;

class Manifest {

	public static function get($file, $type) {
		return self::find($file, $type);
	}

	public static function make($file, $type) {
		$resource_path = base_path(config('gdamd.paths.resources.'.$type));
		$resource_file = $resource_path.'/'.$file;

		$public_path = base_path(config('gdamd.paths.public.'.$type));
		$public_file = $public_path.'/'.$file;

		if (file_exists($resource_file)) {
			if (file_exists($public_path.'/'.$file.'.manifest')) { // check for existing manifest
				$data = json_decode(file_get_contents($public_path.'/'.$file.'.manifest'));

				if (!empty($data->mtime) && !empty($data->current) && !empty($data->original)) {
					if (filemtime($resource_file) > $data->mtime) { //resource file is newer than current
						unlink($public_path.'/'.$file.'.manifest'); //remove the old manifest
						unlink($public_path.'/'.$data['current']); //remove the current public asset
						unlink($public_path.'/'.$data['original']); //remove the original public asset
					} else { //resource file is the same as current, no need to do anything
						return 'No update needed';
					}
				}
			}

			$public_file_data = pathinfo($public_file);

			if (!file_exists($public_file_data['dirname'])) {
				mkdir($public_file_data['dirname']);
			}

			copy($resource_file, $public_file);

			$hash = sha1(file_get_contents($public_file));

			$relative_path = preg_replace('['.$public_path.'/]', '', $public_file_data['dirname']);

			$current = $relative_path.'/'.$public_file_data['filename'].'-'.$hash.'.'.$public_file_data['extension'];

			copy($public_file, $public_path.'/'.$current);

			$size = filesize($public_path.'/'.$current);

			$mtime = filemtime($public_file);

			$manifest = [
				'current' => $current,
				'original' => $file,
				'mtime' => $mtime,
				'hash' => $hash,
				'size' => $size
			];

			file_put_contents($public_path.'/'.$file.'.manifest', json_encode($manifest));

			return 'Created: '.$public_path.'/'.$file.'.manifest';
		}

		return false;
	}

	private static function find($file, $type) {
		$path = config('gdamd.paths.public.'.$type);
		$url = preg_replace('/^public/', '', $path);

		if (file_exists(base_path($path.'/'.$file.'.manifest'))) {
			$data = json_decode(file_get_contents(base_path($path.'/'.$file.'.manifest')));

			if (!empty($data->current)) {
				$url .= '/'.$data->current;
			}
		} else {
			$url .= '/'.$file;
		}

		return $url;
	}

}
