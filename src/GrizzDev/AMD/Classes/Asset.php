<?php

namespace GrizzDev\AMD\Classes;

class Asset {

	public static function css($file) {
		return Manifest::get($file, 'css');
	}

	public static function js($file) {
		return Manifest::get($file, 'js');
	}

}
