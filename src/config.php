<?php

return [
	'paths' => [
		'public' => [
			'css' => 'public/assets/css',
			'js' => 'public/assets/js'
		],
		'resources' => [
			'cs' => 'resources/assets/cs',
			'css' => 'resources/assets/css',
			'js' => 'resources/assets/js',
			'scss' => 'resources/assets/scss',
		]
	]
];
